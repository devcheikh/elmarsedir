from django.urls import path

from elmarse.users.views import (
    user_list_view,
    user_redirect_view,
    user_update_view,
    user_detail_view,
    user_delete_view
)

app_name = "users"
urlpatterns = [
    path("list", view=user_list_view, name="list"),
    path("redirect/", view=user_redirect_view, name="redirect"),
    path("update/<str:pk>/", view=user_update_view, name="update"),
    path("delete/<str:pk>/", view=user_delete_view, name="delete"),
    path("detail/<str:pk>/", view=user_detail_view, name="detail"),
]
