from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin

from django.views.generic import DetailView, ListView, RedirectView,DeleteView, UpdateView
from django.urls import reverse, reverse_lazy
User = get_user_model()


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    slug_field = "pk"
    slug_url_kwarg = "pk"


user_detail_view = UserDetailView.as_view()


class UserListView(LoginRequiredMixin, ListView):

    model = User
    slug_field = "pk"
    slug_url_kwarg = "pk"


user_list_view = UserListView.as_view()


class UserUpdateView(LoginRequiredMixin, UpdateView):

    model = User
    fields = ['first_name', 'last_name', 'email']

    def get_object(self):
        return User.objects.get(pk=self.request.user.pk)


user_update_view = UserUpdateView.as_view()

class UserDelete(DeleteView):
    model = User
    success_url = reverse_lazy('users:list')

user_delete_view = UserDelete.as_view()

class UserRedirectView(LoginRequiredMixin, RedirectView):

    permanent = False

    def get_redirect_url(self):
        return reverse("users:detail", kwargs={"pk": self.request.user.pk})


user_redirect_view = UserRedirectView.as_view()
