# Generated by Django 2.0.8 on 2018-08-29 21:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20180829_2121'),
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('is_hidden', models.BooleanField(default=False)),
                ('category', models.CharField(choices=[('electronic', 'Electronique - إلكتروني'), ('clothes', 'Vêtements - ملابس')], default='clothes', max_length=25)),
                ('image', models.ImageField(upload_to='')),
                ('title', models.TextField(blank=True, default='')),
                ('description', models.TextField(blank=True, default='')),
                ('price', models.IntegerField(verbose_name='Prix')),
                ('shop', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.UserShop')),
            ],
        ),
    ]
