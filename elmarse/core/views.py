from django.shortcuts import render
from django.contrib.auth import get_user_model
from django.views.generic import TemplateView, DetailView, ListView
from .models import Product , UserProfile,UserShop,Chat,Message
from .forms import ProductForm,MessageForm
from django.http import HttpResponseRedirect
User = get_user_model()
from django.db.models import Count
# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic import DetailView
import json
from django.http import HttpResponse
from django.views import View
from django.contrib.contenttypes.models import ContentType
from .models import Activity
from django.shortcuts import redirect
 
# #############################################################################
# #                                 UserShop                                # #
# #############################################################################

class UserShopDetail(DetailView):
    queryset = UserShop.objects.filter(user__is_shopowner=True)

usershop_detail_view = UserShopDetail.as_view()

class UserShopUpdate(UpdateView):
    model = UserShop
    fields = ['name','description','image','avatar','phone','address']
    template_name_suffix = '_update_form'
    def get_success_url(self):
        return self.request.user.get_absolute_url()

usershop_update_view = UserShopUpdate.as_view()

class UserShopList(ListView):
    model = UserShop
    template_name_suffix = '_list'
    context_object_name = "usershop_list"

    def get_queryset(self):
        return UserShop.objects.filter(user__is_shopowner=True)

    def get_context_data(self, **kwargs):
        context = super(UserShopList, self).get_context_data(**kwargs)
        # Create any data and add it to the context
        # context['some_data'] = 'This is just some data'
        return context

usershop_list_view = UserShopList.as_view()

 
class UserShopCreate(CreateView):
    model = UserShop
    fields = ['name','description','image','avatar','phone','address']

    template_name_suffix = '_create_form'
    success_url = reverse_lazy('core:usershop-list')

usershop_create_view = UserShopCreate.as_view()

class UserShopDelete(DeleteView):
    model = UserShop
    success_url = reverse_lazy('core:usershop-list')

usershop_delete_view = UserShopDelete.as_view()

# #############################################################################
# #                                 Products                                # #
# #############################################################################

class ProductDetail(DetailView):
    queryset = Product.objects.annotate(count=Count('activities'))
    def get_context_data(self, **kwargs):

        context = super(ProductDetail, self).get_context_data(**kwargs)
        # Create any data and add it to the context
        user = self.request.user
        activity = Activity.objects.all().\
        filter(content_type__model='product',user=user, activity_type=1).first()
        context['activity'] = activity
        return context

product_detail_view = ProductDetail.as_view()
from django.db.models import Count
from django.db.models import Q
class ProductList(ListView):
    model = Product
    template_name = 'core/product_list.html'
    context_object_name = "product_list"

    def get_products(self):
        user =self.request.user
        products = Product.objects.filter(is_hidden=False,shop__user__is_shopowner=True)
         
        return products

    def get_queryset(self):
        return  self.get_products()
        # {'tags__count': 3}

    def get_context_data(self, **kwargs):
        context = super(ProductList, self).get_context_data(**kwargs)


        return context

product_list_view = ProductList.as_view()
 
class ProductCreate(CreateView):
    model = Product
    fields = ['title','description','image','price','category','is_hidden']

    template_name_suffix = '_create_form'
    success_url = reverse_lazy('core:product-list')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        # things
        self.object.shop = self.request.user.shop
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())
        
   
product_create_view = ProductCreate.as_view()


class ProductUpdate(UpdateView):
    model = Product
    fields =  ['title','description','image','price','category','is_hidden']
    template_name_suffix = '_update_form'
     
    def get_success_url(self):
        return self.request.user.get_absolute_url()


product_update_view = ProductUpdate.as_view()

class ProductDelete(DeleteView):
    model = Product
    success_url = reverse_lazy('core:product-list')

product_delete_view = ProductDelete.as_view()

# #############################################################################
# #                                 Products                                # #
# #############################################################################

def update_profile(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, _('Your profile was successfully updated!'))
            return redirect('settings:profile')
        else:
            messages.error(request, _('Please correct the error below.'))
    else:
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'profiles/profile.html', {
        'user_form': user_form,
        'profile_form': profile_form
    })

def approve(request,user_pk):
    if request.method == 'GET' and request.user.is_superuser:
        user = User.objects.filter(pk=user_pk).first()
        if user:
            user.is_shopowner =True
            user.save()
    
    return redirect('users:list')

approve_view = approve

def revoke(request,user_pk):
    if request.method == 'GET' and request.user.is_superuser:
        user = User.objects.filter(pk=user_pk).first()
        if user:
            user.is_shopowner =False
            user.save()
    
    return redirect('users:list')

revoke_view = revoke