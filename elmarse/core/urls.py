from django.urls import path

from .views import (
    product_list_view,
    product_create_view,
    product_update_view,
    product_delete_view,
    product_detail_view,
    usershop_detail_view,
    usershop_update_view,
    usershop_list_view,
    usershop_create_view,
    usershop_delete_view,
    approve_view,
    revoke_view
)
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

from . import views
from .models import Activity,Product

app_name = "core"



urlpatterns = [
     # #############################################################################
    # #                                 Product                                 # #
    # #############################################################################
    path("product/list", view = product_list_view, name="product-list"),
    path('product/create/', product_create_view, name='product-create'),
    path('product/update/<int:pk>/', product_update_view, name='product-update'),
    path('product/<int:pk>/delete/', product_delete_view, name='product-delete'),
    path('product/detail/<int:pk>/', product_detail_view, name='product-detail'),
    # #############################################################################
    # #                                 UserShop                                # #
    # #############################################################################
    path("shop/list", view = usershop_list_view, name="usershop-list"),
    path('shop/create/', usershop_create_view, name='usershop-create'),
    path('shop/detail/<int:pk>/', usershop_detail_view, name='usershop-detail'),
    path('shop/<int:pk>/delete/', usershop_delete_view, name='usershop-delete'),
    path('shop/update/<int:pk>/', usershop_update_view, name='usershop-update'),
    # #############################################################################
    # #                                 Admin                                 # #
    # #############################################################################
    path('approve/<int:user_pk>/', approve_view, name='user-approve'),
    path('revoke/<int:user_pk>/', revoke_view, name='user-revoke'),
]
