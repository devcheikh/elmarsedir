from django.contrib import admin

from .models import UserShop,UserProfile,Product,Activity,Chat,Message
# Register your models here.
 
@admin.register(UserShop)
class UserShopAdmin(admin.ModelAdmin):
    pass

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    pass

@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    pass
