from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth import get_user_model
from allauth.account.signals import user_signed_up
# Create your models here.
import hashlib
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager
from django.core.mail import send_mail
from django.db import models
from django.dispatch import receiver
from django.utils import timezone
from django.utils.http import urlquote
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse, reverse_lazy
from allauth.account.signals import user_signed_up

User = get_user_model()

ELECTRONIQUE = 'electronic'
CLOTHES = 'clothes'
CATEGORIES = (
    (ELECTRONIQUE, 'Electronique - إلكتروني'),
    (CLOTHES, 'Vêtements - ملابس'),
)
from django.db.models import Sum
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey



# #############################################################################
# #                                 Chat Message                            # #
# #############################################################################
class Chat(models.Model):
    DIALOG = 'D'
    CHAT = 'C'
    CHAT_TYPE_CHOICES = (
        (DIALOG, _('Dialog')),
        (CHAT, _('Chat'))
    )

    type = models.CharField(
        _('Тип'),
        max_length=1,
        choices=CHAT_TYPE_CHOICES,
        default=DIALOG
    )
    members = models.ManyToManyField(User, verbose_name=_("Member"))

    @models.permalink
    def get_absolute_url(self):
        return 'core:chat-messages', (), {'chat_id': self.pk }


class Message(models.Model):
    chat = models.ForeignKey(Chat, verbose_name=_("Chat"),on_delete=models.CASCADE)
    author = models.ForeignKey(User, verbose_name=_("User"),on_delete=models.CASCADE)
    message = models.TextField(_("Message"))
    pub_date = models.DateTimeField(_('Message date'), default=timezone.now)
    is_readed = models.BooleanField(_('Readed'), default=False)

    class Meta:
        ordering=['pub_date']

    def __str__(self):
        return self.message



 # #############################################################################
 # #                              LikeDislike                                # #
 # #############################################################################

class ActivityManager(models.Manager):
    use_for_related_fields = True
    @property
    def likes(self):
        # We take the queryset with records greater than 0
        return self.get_queryset().filter(activity_type=1)
    def liked(self,user):
        # We take the queryset with records greater than 0
        return self.get_queryset().filter(user=user,activity_type=1).first()

    def followers(self):
        # We take the queryset with records less than 0
        return self.get_queryset().filter(activity_type=-1)

    def products(self):
        return self.get_queryset().filter(content_type__model='product')

    def shops(self):
        return self.get_queryset().filter(content_type__model='usershop')


class Activity(models.Model):
    LIKE = 1
    FOLLOW = -1

    ACTIVITIES = (
        (FOLLOW, 'Follow'),
        (LIKE, 'Like')
    )

    activity_type = models.SmallIntegerField(verbose_name=_("activity type"), choices=ACTIVITIES)
    user = models.ForeignKey(User, verbose_name=_("user"),on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')


    objects = ActivityManager()
    def __str__(self):
        return str(self.object_id)
# #############################################################################
# #                                 UserShop                                # #
# #############################################################################

class UserShopManager(models.Manager):
    def get_queryset(self):
        return super(UserShopManager, self).get_queryset().filter(user__is_shopowner=True)

class UserShop(models.Model):
    user = models.OneToOneField(User, primary_key=True, verbose_name= _('User'),
                                related_name='shop',on_delete=models.CASCADE)
    name = models.CharField( _('Shop name'),max_length=100, default = _('Shop name'))
    address = models.CharField( _('Address'),max_length=100, default = _('Shop address'))
    city = models.CharField(_('City'),max_length=100, default='')
    phone = models.IntegerField(_('Phone number'),default=3344556677)
    avatar = models.ImageField(_('Avatar'),blank=True,default='../media/storeavatar.jpg')
    image = models.ImageField( _('Image'),blank=True,default='../media/store.jpg')
    description = models.CharField( _('Description'),max_length=200, default = _('Shop description'))
    followers = models.IntegerField(default=0)
    posts = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add = True,blank=True)
    updated_at = models.DateTimeField(auto_now=True,blank=True)
    activities = GenericRelation(Activity, related_query_name='shops')
    objects = UserShopManager()

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return self.name + self.user.username

    def get_absolute_url(self):
        return reverse('core:usershop-detail', kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('core:usershop-update', kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse('core:usershop-delete', kwargs={'pk': self.pk})

def create_shop(sender, **kwargs):
    if kwargs['created']:
        user_shop = UserShop.objects.create(user=kwargs['instance'],name="Store name")


post_save.connect(create_shop, sender=User)

# #############################################################################
# #                                 UserProfile                             # #
# #############################################################################
def sent(num,body,is_client):
    client = Client(settings.ACCOUNT_SID, settings.AUTH_TOKEN)
    if(is_client):
        msg = client.api.account.messages.create(to = num,from_= 'Ghaza Telecom',body= body)
    else:
        msg = client.api.account.messages.create(to = num,from_= settings.TWILIO_NUM,body= body)
    return msg
class UserProfile(models.Model):
    """Profile data about a user.
    """
    user = models.OneToOneField(User, primary_key=True, verbose_name= _('User'), related_name='profile',
                                on_delete=models.CASCADE)
    avatar_url = models.CharField(max_length=256, blank=True, null=True)
    avatar = models.ImageField( _('Avatar'),blank=True,default='../media/useravatar.jpg')

    #created_at = models.DateTimeField(auto_now_add=True,blank=True)
    #updated_at = models.DateTimeField(auto_now=True,blank=True)
   

    def __str__(self):
        return "he"+self.user.username

    def get_absolute_url(self):
        return reverse('core:userprofile-detail', kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('core:userprofile-update', kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse('core:userprofile-delete', kwargs={'pk': self.pk})
def create_userprofile(sender, **kwargs):
    if kwargs['created']:
        profile = UserProfile.objects.create(user=kwargs['instance'])


post_save.connect(create_userprofile, sender=User)
# #############################################################################
# #                                 Category                                 # #
# #############################################################################

 


# #############################################################################
# #                                 Product                                 # #
# #############################################################################

class Product(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_hidden = models.BooleanField(_('Hidden'),default=True)
    category = models.CharField(max_length=25, choices=CATEGORIES, default=CLOTHES)
    shop = models.ForeignKey(UserShop, on_delete=models.CASCADE,verbose_name= _('User'),related_name='products')
    image = models.ImageField( _('Image'),blank=True,default='../media/food.jpg')
 
    title = models.TextField( _('Title'),blank=True, default='')
    description = models.TextField( _('Description'),blank=True, default='')
    price = models.IntegerField( _('Price'),default=100)
    activities = GenericRelation(Activity, related_query_name='products')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('core:product-detail', kwargs={'pk': self.pk})

    def get_update_url(self):
        return reverse('core:product-update', kwargs={'pk': self.pk})

    def get_delete_url(self):
        return reverse('core:product-delete', kwargs={'pk': self.pk})

    class Meta:
        ordering = ['-created_at']



# #############################################################################
# #                                 Login                                  # #
# #############################################################################

@receiver(user_signed_up)
def set_initial_user_names(request, user, sociallogin=None, **kwargs):
    """
    When a social account is created successfully and this signal is received,
    django-allauth passes in the sociallogin param, giving access to metadata on the remote account, e.g.:

    sociallogin.account.provider  # e.g. 'twitter'
    sociallogin.account.get_avatar_url()
    sociallogin.account.get_profile_url()
    sociallogin.account.extra_data['screen_name']

    See the socialaccount_socialaccount table for more in the 'extra_data' field.
    From http://birdhouse.org/blog/2013/12/03/django-allauth-retrieve-firstlast-names-from-fb-twitter-google/comment-page-1/
    """

    preferred_avatar_size_pixels = 256

    picture_url = "http://www.gravatar.com/avatar/{0}?s={1}".format(
        hashlib.md5(user.email.encode('UTF-8')).hexdigest(),
        preferred_avatar_size_pixels
    )

    if sociallogin:
        # Extract first / last names from social nets and store on User record
        if sociallogin.account.provider == 'twitter':
            name = sociallogin.account.extra_data['name']
            user.first_name = name.split()[0]
            user.last_name = name.split()[1]
            user.display_name = name
            picture_url = sociallogin.account.extra_data['profile_image_url_https']


        if sociallogin.account.provider == 'facebook':
            user.first_name = sociallogin.account.extra_data['first_name']
            user.last_name = sociallogin.account.extra_data['last_name']
            # verified = sociallogin.account.extra_data['verified']
            picture_url = "http://graph.facebook.com/{0}/picture?width={1}&height={1}".format(
                sociallogin.account.uid, preferred_avatar_size_pixels)

        if sociallogin.account.provider == 'google':
            user.first_name = sociallogin.account.extra_data['given_name']
            user.last_name = sociallogin.account.extra_data['family_name']
            # verified = sociallogin.account.extra_data['verified_email']
            picture_url = sociallogin.account.extra_data['picture']
     

    profile = UserProfile(user=user, avatar_url=picture_url)
    profile.save()
    #
    # user.guess_display_name()
    user.save()

